
import pandas as pd

guion = lambda: print('-------------------------------------------')

#Crear serie
serie = pd.Series( [10,20,30,40,50,60], index=['Enero','Febrero','Marzo','Abril','Mayo','Junio'] )
print(serie)

guion()
print(serie[0])
print(serie['Enero'])

guion()
#Obtener el objeto que contiene los indices de la serie
print( serie.index )
serie.index = ['ene','feb','mar','abr','may','jun']
print(serie)

guion()
print( serie.dtype )
guion()
print(serie.values)

guion()
print( serie.axes )
print( serie.shape )

guion()
serie.index.name = 'Meses'
serie.name = 'Ventas del año 2021'
print(serie)


guion()
print('DATAFRAME')
guion()

dict_ventas = {
    'frutas': [100,90,150,200],
    'aseo': [80,120,250,350]
}
ventas = pd.DataFrame(dict_ventas, index=['ene','feb','mar','abr'])

print(ventas)

guion()
print( ventas.index )
guion()
print(ventas.columns)
guion()
print( ventas.axes )
print( ventas.shape )

