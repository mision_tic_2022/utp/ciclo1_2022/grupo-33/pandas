import pandas as pd
import numpy as np

guion = lambda: print('------------------------------------')

movies = pd.read_csv('movies.csv')

print( movies.info() )

guion()
guion()
print( movies.describe() )

guion()
guion()
title = movies['Title']
print(title)
guion()
guion()
#Obtener los primeros 5 registros
print( title.head() )
#Obtener los último 5 registros
print( title.tail() )
guion()
#Obtener los primeros 10 registros
print( title.head(10) )

guion()
guion()
#Obtener una muestra del dataframe (selecciona registros de manera aleatoria)
print( movies.sample(5) )
guion()
guion()
guion()
guion()

subDataFrame = movies.loc[:, ['Title','Facebook Likes - Director','Facebook Likes - Actor 1','Facebook likes - Movie'] ]
#print(subDataFrame.info())
print( subDataFrame.head() )
#Crear columna
subDataFrame['Total likes'] = subDataFrame['Facebook Likes - Director'] + subDataFrame['Facebook Likes - Actor 1'] + subDataFrame['Facebook likes - Movie']
print( subDataFrame.head() )
print( subDataFrame.describe() )

guion()
guion()
peliculas_director = movies.loc[:, ["Director", "Title", "Genres", "Language"]]
#print(peliculas_director.info())
#Crear tabla pivote / tabla dinámica -> np.size -> retorna cuantas veces se repite el director
tabla = pd.pivot_table( peliculas_director, index=['Director'], aggfunc=[np.size] )
print(tabla.head())